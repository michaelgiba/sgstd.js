var mongojs = require("mongojs");
var db = mongojs('sgtsd');


/* Constant and Utility functions */

var usersCollection = db.collection("sgtsdUsers");


var USER_HISTORY_LIMIT = 10;
debug = false;






/* ---------------------------- */

function sjlog(string){
	if(debug){
		var prefix = "sjstd: ";
		console.log(prefix + string)
	}
}

/*  - - - - - - - - - - - - - -  */


function convertDocsToUser(inputs){
	this["USER_DATA"] = {};
	for(var key in inputs){
		this["USER_DATA"][key] = inputs[key];
	}
	return addFunctionsToUser(this);
}

function User(key,value){
	var k = {};
	k[key] = value;
	return User(k);
}
function User(KeyValueDict){
	KeyValueDict["PreviousContent"] = []
	return addFunctionsToUser(KeyValueDict)
}
function addFunctionsToUser(nonFunctionalUser){
	nonFunctionalUser.contentAccessed = contentAcessed;
	//
	return nonFunctionalUser
}
function NewUserAction(User,UUID,callback){
	User["USER_DATA"]["PreviousContent"].push(UUID);
	callback(UserToDocs(User));	
}
function UserToDocs(user){
	return user["USER_DATA"]
}






function strangeSearch(counts,currentIndex,newCount){
	
	var index1 = 0;
	var index2 = currentIndex;
	var middle;
	var middleElem;

	while (index1 != index2 && middle != 0){
		middle = (index1 + index2)/2 | 0;
		middleElem = counts[middle]
		if(newCount > middleElem){
			index2 = middle;
		}
		else{
			index1 = middle + 1;
		}
	}
	return index1;
	
}


function insertNewUUID(dictionary,values,counts,UUID){
	var exportData = {};
	
	if(dictionary[UUID]){
		
		var oldCount = dictionary[UUID]["Count"];
		var newCount = oldCount + 1;
		var currentIndex = dictionary[UUID]["Index"];		
		var newIndex = strangeSearch(counts,currentIndex,newCount);
		var uuidAtSwapIndex = values[newIndex];
		var countAtSwapIndex = counts[newIndex];

		dictionary[uuidAtSwapIndex]["Index"] = currentIndex;
		dictionary[UUID]["Index"] = newIndex;
		

		counts[currentIndex] = countAtSwapIndex;
		values[newIndex] = values[currentIndex];
		values[currentIndex] = uuidAtSwapIndex;
		counts[newIndex] = newCount;
		dictionary[UUID]["Count"] = newCount; 
	
	
	}
	else{

		dictionary[UUID] = {};
		dictionary[UUID]["Count"] = 1;
		dictionary[UUID]["Index"] = values.length;
		values.push(UUID);
		counts.push(1);

	}

	exportData["dictionaryOfRelatedContent"] = dictionary;
	exportData["relatedContentValues"] = values;
	exportData["relatedContentCounts"] = counts;

	return exportData;
}


function contentAcessed(UUID,collection,usersTopOrMostRecentUUID,callback){
	sjlog(UUID  +" "+collection);
	collection.findOne({ "UUID" : UUID },function(err,docs){

		if(!err){
			if(docs){
				if(docs["UUID"] == usersTopOrMostRecentUUID){
					callback(false);
					return;	
				} 				
				var nDocs = docs;
		
				var relatedContent = docs["relatedContent"];
				var hashMapOfUsersPrevious = relatedContent["dictionaryOfRelatedContent"];
				var values = relatedContent["relatedContentValues"];
				var counts = relatedContent["relatedContentCounts"];

				nDocs["relatedContent"] = insertNewUUID(hashMapOfUsersPrevious,values,counts,usersTopOrMostRecentUUID);	

				collection.update({ "UUID" : UUID }, nDocs ,function(err){
					if(!err) callback(true);
					else callback(false);
				});

			}
			else{

				var content = {};
				content["UUID"] = UUID;
				content["relatedContent"] = RelatedContentShell(usersTopOrMostRecentUUID);
				collection.insert(content);

				callback(true);
			}
		}
		else{
			sjlog("Error connecting to " + this.collection);
			callback(false);
		}
	});
};


function RelatedContentShell(firstUUID){
	analyticsDictionary = {};
	var relatedContent = {};

	relatedContent[firstUUID] = {"Count":1,"Index":0};

	analyticsDictionary.dictionaryOfRelatedContent = relatedContent;
	analyticsDictionary.relatedContentValues = [firstUUID];
	analyticsDictionary.relatedContentCounts = [1];

	return analyticsDictionary;
}


//Create User

var createUser = function(value,callback){
	var json = {};
	
	json["Username"] = value
	json["PreviousContent"] = [];
	usersCollection.findOne({"Username":value},function(err,docs){
		sjlog("Adding new user");
		if(!err){
			if(docs){
				callback(false);
			}
			else{
				usersCollection.insert(json,function(){
					callback(json);
				});
			}
		}
		else sjlog("Error loading User Collection");

	});
};

//Return User

var getUser = function(value,callback){
	var json = {};
	json["Username"] = value;
	usersCollection.findOne(json,function(err,docs){
		if(!err){
			if(docs != undefined && docs){
				var user = convertDocsToUser(docs);
				callback(user);
			}
			else{
				createUser(value,function(user){
					callback(convertDocsToUser(user));
				});
			}
		}
	});
};


var userAccessedContent = function(user,UUID){
	var localCollection = this.collection;
	getUser(user,function(returnedUser){
		if(returnedUser){
			var lastContent = returnedUser["USER_DATA"]["PreviousContent"][returnedUser["USER_DATA"]["PreviousContent"].length-1]
			if(!lastContent) lastContent = UUID;
			returnedUser.contentAccessed(UUID,localCollection,lastContent,function(allGood){
				if(allGood){
					sjlog("Update new content to users previous");
					usersCollection.update({ "Username": returnedUser["USER_DATA"]["Username"]},{
					 	$push:{
					 		"PreviousContent":{ 
					 			$each: [UUID], 
					 			$slice: -1*USER_HISTORY_LIMIT
					 		}
						} 
					});
				}
				else sjlog("Repeated Same Content dont update");
			});
		}
		else sjlog("no user exists");
	});
}




var getContentMostRelated = function(contentUUID,maxNumber,callback){
	var collection = this.collection;
	collection.findOne({"UUID":contentUUID},function(err,docs){
		if(!err){
			if(docs){
				callback(docs["relatedContent"]["relatedContentValues"].slice(0,maxNumber));
			}
			else{
				sjlog("user doesnt exist");
				callback(null);
			}
		}
		else{
			callback(null);
			sjlog("theres been an error with mongo");
		}
	});
};


var getUsersPreviousContentAccessed = function(Username,number,callback){
	var collection = this.collection;
	usersCollection.findOne({"Username":Username},function(err,docs){
		if(!err){
			if(docs){

				callback(docs["PreviousContent"].slice(0,number));
			}
			else{
				callback(false);
				sjlog("no user found");
			}
		}
		else{
			callback(false);
			sjlog(err);
		}
	})
};







exports.contentTracker =  function(contentObject,enableUserTracking){
	
	if(!contentObject) return;
	if(!enableUserTracking) this.accessedUsers = true;

	this.collection = db.collection(contentObject);	
	this.userAccessedContent = userAccessedContent;
	this.createUser = createUser;
	this.getRelatedContent = getContentMostRelated;
	this.getUsersPreviousContent = getUsersPreviousContentAccessed;

	return this;
}




