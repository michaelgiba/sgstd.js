var s = require("./sjstd");


//First Simply pick the group of data which you would want to relate together.

var tracker = s.contentTracker("ShoppingCart");

//Lazy instantiation of users, completely seperate from account data.

// Using this dynamically allows one to build relationships between content over time.

tracker.userAccessedContent("MichaelG","ARandomVideoUUID");

tracker.getRelatedContent("AnotherRandomUUID", 20 ,function(suggestedContent){

	/*
		suggested Content would be null if no suggested content is availible, otherwise it returns the most popular
		or more recent content visited that is related to this one.

		no duplicates.
	*/

});

tracker.getUsersPreviousContent("Cookyabig",20,function(err){
	/*
		Returns the uuid's of previously visited content
	*/
});








