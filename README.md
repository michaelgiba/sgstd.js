Sgstd.js
========

A high-level platform for making related content on node.js web applications.

This project is intended for providing suggestions for users of a website based on their previous interacting with content and for generating relationships between content. 

##Usage

First Simply pick the group of data which you would want to relate together. In this example we will be relating shopping items.

```javascript
var s = require("./sjstd");
var tracker = s.contentTracker("ShoppingItems");

```

Then without any other information needed except a Username and a UUID for an item of content accessed, Sgstd maps and relates content together based on what that user previously visited. To do this we show:

```javascript

tracker.userAccessedContent("MichaelG","ARandomVideoUUID");

tracker.getRelatedContent("AnotherRandomUUID", 20 ,function(suggestedContent){

	/*
		suggested Content would be null if no suggested content is availible, otherwise it returns
		an array of most popular or more recent content visited that is related to 'AnotherRandomUUID'

		no duplicates will appear
	*/

});

```
We can also return a users history up to a specified number of content UUID's.

```javascript


tracker.getUsersPreviousContent("Cookyabig",20,function(err){
	/*
		Returns the uuid's of previously visited content
	*/
});

```

=========

###Contribute
Anyone can contribute that wants to.

